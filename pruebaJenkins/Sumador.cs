﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pruebaJenkins
{
    public class Sumador
    {

        public int sumarDosInt(int a, int b)
        {
            return a + b;
        }

        public string JsonNumber(int num)
        {
            return JsonConvert.SerializeObject(num);
        }

    }
}
