﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using pruebaJenkins;

namespace TestPruebaJenkins
{
    [TestClass]
    public class TestSumador
    {
        [TestMethod]
        public void Test_can_Add_2_3()
        {
            Sumador sum = new Sumador();
            int resultado = sum.sumarDosInt(2, 3);

            Assert.AreEqual<int>(5, resultado);

        }

        [TestMethod]//enzo
        public void Test_can_Serialize_3()
        {
            Sumador sum = new Sumador();
            string resultado = sum.JsonNumber(3);

            Assert.AreEqual<string>("3", resultado);
        }
        /*[TestMethod]
        public void Test_can_Add_2_4()//pepe9
        {
            //comentario push push pushh
            Sumador sum = new Sumador();
            int resultado = sum.sumarDosInt(2, 4);

            Assert.AreEqual<int>(5, resultado);

        }*/
    }

}
